from os import listdir
import os
import datetime

class WordcountEntry():
    def __init__(self, path):
        def parse_wc_from_line(line: str):
            return int(line.split(":")[1])

        self.words_in_text = 0
        self.words_in_headers = 0
        self.words_outside_text = 0
        self.nro_of_headers = 0
        self.nro_of_floats_tables_figures = 0
        self.nro_of_math_inlines = 0
        self.nro_of_math_displayed = 0
        self.files = 0

        # Parse date
        fname = os.path.basename(path)
        date = datetime.datetime.strptime(fname, 'wc-%Y-%m-%d.txt')
        self.date = date

        with open(path, 'r') as f:
            lines = f.readlines()

        found_section = False
        for line in lines:
            line = line.strip()

            if line == "Sum of files: main.tex":
                found_section = True

            if found_section:
                if line.startswith("Words in text:"):
                    self.words_in_text = parse_wc_from_line(line)
                elif line.startswith("Words in headers:"):
                    self.words_in_headers = parse_wc_from_line(line)
                elif line.startswith("Words outside text (captions, etc.):"):
                    self.words_outside_text = parse_wc_from_line(line)
                elif line.startswith("Number of headers:"):
                    self.nro_of_headers = parse_wc_from_line(line)
                elif line.startswith("Number of floats/tables/figures:"):
                    self.nro_of_floats_tables_figures = parse_wc_from_line(line)
                elif line.startswith("Number of math inlines:"):
                    self.nro_of_math_inlines = parse_wc_from_line(line)
                elif line.startswith("Number of math displayed:"):
                    self.nro_of_math_displayed = parse_wc_from_line(line)


    def __str__(self):
        return f"Words in text: {self.words_in_text}\n" + \
            f"Words in headers: {self.words_in_headers}\n" + \
            f"Words outside text (captions, etc.): {self.words_outside_text}\n" + \
            f"Number of headers: {self.nro_of_headers}\n" + \
            f"Number of floats/tables/figures: {self.nro_of_floats_tables_figures}\n" + \
            f"Number of math inlines: {self.nro_of_math_inlines}\n" + \
            f"Number of math displayed: {self.nro_of_math_displayed}\n"

def get_paths():
    path = "wordcounts"
    files = [os.path.join(path, f) for f in listdir(path) if os.path.isfile(os.path.join(path, f))]
    return files

def draw(wcs):
    """ Takes list of WordcountEntries as parameter """

    # Multiple scales into one axis
    # https://matplotlib.org/stable/gallery/subplots_axes_and_figures/two_scales.html

    import matplotlib.pyplot as plt

    ax1_color = 'magenta'

    fig, ax1 = plt.subplots(layout='constrained', figsize=(19.20,10.80))
    ax1.set_xlabel('time')
    # ax1.tick_params(axis='y', labelcolor=ax1_color)

    ax2 = ax1.twinx()
    ax1.set_ylabel('All other graphs', color=ax1_color)
    ax2.set_ylabel('Words in text (magenta)', color=ax1_color)

    # https://matplotlib.org/stable/gallery/color/named_colors.html
    axis1_color = "gray"
    axis2_color = "lightgray"
    # https://stackoverflow.com/questions/1982770/changing-the-color-of-an-axis#12059429
    fig.patch.set_facecolor('black')
    ax1.grid(color=axis1_color)
    ax1.spines['bottom'].set_color(axis1_color)
    ax1.spines['top'].set_color(axis1_color) 
    ax1.spines['right'].set_color(axis1_color)
    ax1.spines['left'].set_color(axis1_color)
    ax1.tick_params(axis='x', colors=axis1_color)
    ax1.tick_params(axis='y', colors=axis1_color)
    ax1.yaxis.label.set_color(axis1_color)
    ax1.xaxis.label.set_color(axis1_color)
    ax1.title.set_color(axis1_color)
    ax1.set_facecolor('black')
    ax1.set_facecolor('black')
    # ax2.grid(color=axis2_color)
    ax2.spines['bottom'].set_color(axis2_color)
    ax2.spines['top'].set_color(axis2_color) 
    ax2.spines['right'].set_color(axis2_color)
    ax2.spines['left'].set_color(axis2_color)
    ax2.tick_params(axis='x', colors=axis2_color)
    ax2.tick_params(axis='y', colors=axis2_color)
    ax2.yaxis.label.set_color(axis2_color)
    ax2.xaxis.label.set_color(axis2_color)
    ax2.title.set_color(axis2_color)
    ax2.set_facecolor('black')
    ax2.set_facecolor('black')

    dates = []
    words_in_text = []
    words_in_headers = []
    words_outside_text = []
    nro_of_headers = []
    nro_of_floats_tables_figures = []
    nro_of_math_inlines = []
    nro_of_math_displayed = []
    files = []
    for wc in wcs:
        dates.append(wc.date)
        words_in_text.append(wc.words_in_text)
        words_in_headers.append(wc.words_in_headers)
        words_outside_text.append(wc.words_outside_text)
        nro_of_headers.append(wc.nro_of_headers)
        nro_of_floats_tables_figures.append(wc.nro_of_floats_tables_figures)
        nro_of_math_inlines.append(wc.nro_of_math_inlines)
        nro_of_math_displayed.append(wc.nro_of_math_displayed)

    # Plot Axis 1
    ax1.plot(dates, words_in_headers, label="Words in headers")
    ax1.plot(dates, words_outside_text, label="Words outside text")
    ax1.plot(dates, nro_of_headers, label="Nro of headers")
    ax1.plot(dates, nro_of_floats_tables_figures, label="Nro of floats, tables and figures")
    ax1.plot(dates, nro_of_math_inlines, label="Nro of math inlines")
    ax1.plot(dates, nro_of_math_displayed, label="Nro of math displayed")
    ax1.legend(loc="upper left")

    # Plot Axis 2
    ax2.plot(dates, words_in_text, color=ax1_color, label="Words in text")
    ax2.legend(loc="upper right")

    # fig.tight_layout()
    plt.savefig("img/words.png", format="png")
    # plt.show()


def main():
    files = get_paths()
    wcs = []
    for file in files:
        wc = WordcountEntry(file)
        wcs.append(wc)

    print(len(wcs))
    draw(wcs)

if __name__ == "__main__":
    main()


