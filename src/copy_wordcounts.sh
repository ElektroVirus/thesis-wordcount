#!/bin/bash
set -euo pipefail
rsync -a ~/TTY/Dippa/wordcounts/ ~/Programming/Python/thesis-wordcount/wordcounts/
python ~/Programming/Python/thesis-wordcount/src/main.py
rsync img/words.png ~/TTY/Dippa/
