# Thesis wordcount

This is a simple project to draw wordcount graphs generated from my masters
thesis LaTeX source code using texcount.

The outputs of textcount must be in a directory 'wordcounts' and named as
so that Python strptime will understand it with 'wc-%Y-%m-%d.txt', e.g.
'wc-2023-10-18.txt'.

## How to run

```
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
python src/main.py
```

## Example

![Example screenshot](img/example.png "Example screenshot")
